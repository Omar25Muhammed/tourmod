from erpnext.hr.doctype.employee.employee import Employee
import frappe
from frappe.model.document import Document
from frappe import _
from tourmod.api import grades as grds

class Employee(Document):

    def after_insert(self):
        grds()