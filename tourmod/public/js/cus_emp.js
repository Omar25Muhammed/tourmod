// Copyright (c) 2023, Me and contributors
// For license information, please see license.txt

frappe.ui.form.on('Employee', {
	refresh: function (frm) {
		// frappe.msgprint('works')
	},
	validate: function (frm) {
		if (frm.doc.grade == 0) {
			frm.doc.grade = ''
			frm.refresh_field('grade')
			frappe.throw('Invalid!')
		}
		else if ((frm.doc.grade > 0) && (frm.doc.grade <= 10)) {
			frm.doc.after = 4
			frm.refresh_field('after')
		}
		else if ((frm.doc.grade > 10) && (frm.doc.grade <= 11)) {
			frm.doc.after = 5
			frm.refresh_field('after')
		}
		else {
			frm.doc.after = 1
			frm.refresh_field('after')
		}
	}
});
