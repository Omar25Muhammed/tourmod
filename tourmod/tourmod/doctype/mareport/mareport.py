# Copyright (c) 2023, Me and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from tourmod.api import grades


class MaReport(Document):

    def after_insert(self):
        grades()
