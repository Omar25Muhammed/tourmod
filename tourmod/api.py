import frappe


def grades():
    try:
        import datetime
        docs = frappe.get_list('Employee', pluck='name')
        # c = 0

        def due(start_date, end_date):
            number_of_days = []
            delta = datetime.timedelta(days=1)
            if end_date < start_date:
                start_date, end_date = end_date, start_date
                while start_date < end_date:
                    start_date += delta
                    number_of_days.append(start_date)
                return -len(number_of_days)

            else:
                while start_date < end_date:
                    start_date += delta
                    number_of_days.append(start_date)
                return len(number_of_days)
    except:
        ...

    # for i in docs:
    # 	c = int(frappe.db.get_value('Employee', i, 'number_of_days_left')) + 1
    def period_up(start_date, end_date):
        if due(start_date, end_date) <= 30 and due(start_date, end_date) >= -30:
            return 30

        # def s60een(start_date, end_date):
        if due(start_date, end_date) <= 60 and due(start_date, end_date) >= -60 \
                and not (due(start_date, end_date) <= 30 and due(start_date, end_date) >= -30):
            return 60

        # def n90een(start_date, end_date):
        if due(start_date, end_date) <= 90 and due(start_date, end_date) >= -90 \
                and not (due(start_date, end_date) <= 60 and due(start_date, end_date) >= -60) \
        and not (due(start_date, end_date) <= 30 and due(start_date, end_date) >= -30):
            return 90

        # def 270(start_date, end_date):
        if due(start_date, end_date) <= 270 and due(start_date, end_date) >= -270 \
                and not (due(start_date, end_date) <= 90 and due(start_date, end_date) >= -90) \
                and not (due(start_date, end_date) <= 60 and due(start_date, end_date) >= -60) \
        and not (due(start_date, end_date) <= 30 and due(start_date, end_date) >= -30):
            return 270

        # def 360(start_date, end_date):
        if due(start_date, end_date) <= 360 and due(start_date, end_date) >= -360 \
                and not (due(start_date, end_date) <= 270 and due(start_date, end_date) >= -270) \
                and not (due(start_date, end_date) <= 90 and due(start_date, end_date) >= -90) \
                and not (due(start_date, end_date) <= 60 and due(start_date, end_date) >= -60) \
        and not (due(start_date, end_date) <= 30 and due(start_date, end_date) >= -30):
            return 360

    for i in docs:
        try:
            after = int(frappe.db.get_value('Employee', i, 'after'))
        except:
            # after = 0
            continue
        end_date = datetime.date(
            day=1,
            month=int(frappe.db.get_value('Employee', i, 'month_of_earning')),
            year=int(frappe.db.get_value('Employee', i, 'year_of_earning')) + after)

        start_date = datetime.date.today()
        frappe.db.set_value(
            'Employee', i, 'number_of_days_left', due(start_date, end_date))
        frappe.db.set_value('Employee', i, 'filter_on',
                            period_up(start_date, end_date))

    frappe.db.commit()
